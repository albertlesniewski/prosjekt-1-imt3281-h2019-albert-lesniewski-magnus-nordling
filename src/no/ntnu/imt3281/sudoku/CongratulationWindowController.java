package no.ntnu.imt3281.sudoku;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.ResourceBundle;


/**
 * Controller class for the congratulations window that pops out when the user solves the puzzle
 */
public class CongratulationWindowController {

    @FXML
    Label congratsLabel;

    @FXML
    Label timeLabel;

    @FXML
    Button newGameButton;

    @FXML
    Button exitButton;

    @FXML
    BorderPane root;

    private SudokuLogic logic;
    private ResourceBundle resourceBundle;

    /**
     * Initialises GUI and gets time user used to solve the puzzle
     */
    @FXML
    void initialize(){
        logic = Sudoku.getLogic();
        resourceBundle = ResourceBundle.getBundle("Messages", Sudoku.getCurrentLocale());

        int timePlayed = logic.getTimeElapsed();

        int minutesPlayed = timePlayed / 60;
        int secondsPlayed = timePlayed - (minutesPlayed * 60);

        congratsLabel.setText(resourceBundle.getString("victory"));

        timeLabel.setText(resourceBundle.getString("TimePlayedText") + minutesPlayed +":" + secondsPlayed);

        newGameButton.setText(resourceBundle.getString("buttonNewGame"));
        exitButton.setText(resourceBundle.getString("buttonExit"));
    }

    /**
     * Is called when user click on newGameButton button, so that the congratulation window will close and new game will start
     * @param event Information about the event
     */
    @FXML
    public void newGameButtonPressed(ActionEvent event){
        Node source = (Node) event.getSource();
        Stage stage = (Stage)source.getScene().getWindow();
        stage.close();
    }

    /**
     * Closes application
     * @param event Information about the event
     */
    @FXML
    public void exitButtonPressed(ActionEvent event){
        System.exit(0);
    }
}
