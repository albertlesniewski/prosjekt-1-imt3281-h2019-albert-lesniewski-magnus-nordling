package no.ntnu.imt3281.sudoku;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.LogManager;

/**
 * Class that logs objects.
 *
 *  Based of the work of O. Kolloen.
 *  Original description:
 *  * Class to get a Logger object, initialized with the properties from config/logging.properties.
 *  * Designed with the factory pattern/singleton pattern.
 *  *
 *  * @author IMT3281
 */
public class SudokuLogger {
    private static Logger LOGGER = null ;
    private static SudokuLogger sudokuLogger = new SudokuLogger();

    /**
     *  function used to access the LOGGER object.
     *  Will either return an LOGGER object it already exist, or it will create new instance
     *      and load setting from config file before returning.
     * @return Logger object to collect logs of
     */
    public static Logger getLogger(){
        // Checks if an instance already exists. Use of Singleton.
        if (LOGGER != null){
            return LOGGER;
        }
        //Initializes a new instance of Logger object
        LOGGER = Logger.getLogger("primaryLogger");

        LOGGER.setLevel(Level.ALL);
        LOGGER.info("initializing - trying to load configuration");
        try{
            // get handle to property file, then read and parse
            InputStream inputStream = sudokuLogger.getClass().getResourceAsStream("/config/logging.properties");
            LogManager.getLogManager().readConfiguration(inputStream);
        } catch (IOException e){
            System.err.println("Can not read config file");
            System.err.println("Console only");
        }
        LOGGER.info("Logging initialized");
        return LOGGER;
    }
}
