package no.ntnu.imt3281.sudoku;

/**
 *  Class that handles input errors from the textfields.
 *  The class is used when sending a message about when an input is not between 1 - 9.
 *
 *  This class is currently not in use due to giving textField REGEX-functionality
 *      when creating them in SudokuController.initialize().
 */
public class BadNumberException extends Exception {
    private final int row;
    private final int column;
    private final int number;
    private final String errorMessage;

    /**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param  row  coordinate x til cell.
     * @param  column  coordinate y til cell.
     * @param  number user input number.
     * @param  errorMessage Message that tell what the error is.
     */
    public BadNumberException(int row, int column, int number, String errorMessage) {
        super();
        this.row = row;
        this.column = column;
        this.number = number;
        this.errorMessage = errorMessage;
    }

    /**
     *
     * @return String which show cell position and what
     */
    @Override
    public String toString() {
        return "BadNumberException{" +
                "ErrorMessage='" + errorMessage + '\'' +
                ", row=" + row +
                ", column=" + column +
                ", number=" + number +
                '}';
    }
}
