package no.ntnu.imt3281.sudoku;


import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;


/**
 * <p>SudokuLogic class - controls whole logic of Sudoku application. </p>
 */

public class SudokuLogic {
    private static final Logger LOGGER = SudokuLogger.getLogger();
    private static final int GRID_SIZE = 9;
    private static final int PANE_SIZE = 3;
    private static final int ARRLEN = GRID_SIZE - 1;
    private Timestamp startTime;


    private int[][] sudokuBoard = new int[GRID_SIZE][GRID_SIZE];
    private int[][] lockedBoard = new int[GRID_SIZE][GRID_SIZE];

    // Used in randomization of board. Put here by recommendation of SonarCube
    private Random random = new Random();

    private static final String BOARD = "board";

    /**
     * <p>Default Constructor - Sets up board and Grid size, loads default board, scramble the board and fills the grids</p>
     * @see SudokuLogic#loadBoard(String), where the string parameter is the location of the file.
     * Starts a timer to time how long it takes for a user to solve the game.
     */
    public SudokuLogic(){
        loadBoard("board1.json");

        generateLockedBoard();

        startTime = new Timestamp(new Date().getTime());
    }

    /**
     * Constructor that loads a board from a json file based on selected game difficulty level.
     * Called from {@link SudokuController#initialize()}.
     * Changes the layout of the board:
     * @see #boardPositionScrambler().
     * Randomizes the board:
     * @see #randomizeBoardNumbers().
     * @param diffLevel the difficulty level number.
     */
    public SudokuLogic(int diffLevel){
        startTime = new Timestamp(new Date().getTime());

        if(diffLevel == 0){
            loadBoard("boards_easy.json");
        } else if(diffLevel == 1){
            loadBoard("boards_medium.json");
        } else if(diffLevel == 2){
            loadBoard("boards_hard.json");
        } else{
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.warning("Wrong difficulty level. Will Load easy difficulty board as default");
            LOGGER.log(Level.SEVERE, "Wrong difficulty level");
            loadBoard("boards_easy.json");
        }
        boardPositionScrambler();
        randomizeBoardNumbers();
        generateLockedBoard();
    }

    /**
     * Calculates the time it took a user to solve the sudoku game.
     * Called from {@link CongratulationWindowController#initialize()}.
     * @return time took to solve the game in seconds.
     */
    public int getTimeElapsed(){
        Timestamp endTime = new Timestamp(new Date().getTime());
        long milisec = endTime.getTime() - startTime.getTime();
        return ((int) milisec / 1000);
    }

    /**
     * Loads board template from json file and fills sudokuBoard array.
     * Called from the class constructors.
     * @see #SudokuLogic().
     * @see #SudokuLogic(int).
     * @param jsonBoard String that specifies which file we want to load board from
     */
    public void loadBoard(String jsonBoard){
        try( BufferedReader reader = new BufferedReader(new InputStreamReader(SudokuLogic.class.getResourceAsStream("SudokuBoards/" + jsonBoard)))){
            StringBuilder localJsonBuilder = new StringBuilder();
            String currentLine = null;
            while((currentLine = reader.readLine()) != null){
                localJsonBuilder.append(currentLine);
            }
            JSONObject obj = new JSONObject(localJsonBuilder.toString());
            int numBoards = obj.getInt("boardNum");
            String boardName = BOARD;
            if(numBoards > 1){
                boardName = boardName + Integer.toString(random.nextInt((numBoards - 1) + 1) + 1);
            } else {
                boardName = boardName + "1";
            }

            JSONArray jsonArray = obj.getJSONArray(boardName);
            for(int row = 0; row < GRID_SIZE; row++){
                JSONArray squareArray = jsonArray.getJSONArray(row);
                for(int column = 0; column < GRID_SIZE; column++){
                    sudokuBoard[row][column] = squareArray.getInt(column);
                }
            }
        } catch (IOException err){
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.warning("Could not load the board");
            LOGGER.log(Level.SEVERE, err.getMessage(), err);
        }
    }

    /**
     * Creates a  copy of the sudoku board array.
     * @see #SudokuLogic().
     */
    private void generateLockedBoard(){
        for (int row = 0; row < GRID_SIZE; row++){
            for(int column = 0; column < GRID_SIZE; column++){
                lockedBoard[row][column] = sudokuBoard[row][column];
            }
        }
    }

    /**
     * Checks if a user has finished the sudoku game.
     * Called from {@link SudokuController#inputControl(TextField, TextArea, int, int)}.
     * @return bool, false if : a cell is -1 or if a number is duplicated in a row, column or grid, true otherwise.
     */
    public boolean validateSudokuBoard(){
        for(int row = 0; row < GRID_SIZE; row++){
            for(int column = 0; column < GRID_SIZE; column++){
                if(sudokuBoard[row][column] < 0){
                    return false;
                } else{
                    if(this.inRow(row, column, Integer.toString(sudokuBoard[row][column])) || this.inColumn(row, column, Integer.toString(sudokuBoard[row][column])) || this.inGrid(row, column, Integer.toString(sudokuBoard[row][column]))){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Returns the sudoku board size 81.
     * Used for testing.
     * @return integer board size.
     */
    public int getBoardSize(){
        int numCells = sudokuBoard.length;
        int cellSize = sudokuBoard[0].length;
        return numCells*cellSize;
    }


    /**
     * Updated the board array after a valid user input.
     * Called from {@link SudokuController#inputControl(TextField, TextArea, int, int)}.
     * @param row coordinate x til cell.
     * @param column coordinate y til cell.
     * @param input user input number as a string.
     */
    protected void updateBoard(int row, int column, String input) throws BoardConsistencyException {
            checkIfPosNotChangeble(row, column);
            sudokuBoard[row][column] = Integer.parseInt(input);
    }

    /**
     * Gets the sudoku board array.
     * Called from {@link SudokuController#initialize()}.
     * @return the sudoku board array.
     *
     */
    public int[][] getBoard(){
        // added clone() due to recommendation from sonarQube server.
        return sudokuBoard.clone();
    }

    public int[][] getLockedBoard(){
        // added clone() due to recommendation from sonarQube server.
        return  lockedBoard.clone();
    }



    /**
     * Checks if a user input number is present in the row.
     * Called from {@link SudokuController#inputControl(TextField, TextArea, int, int)}.
     * @param rowIdx coordinate x of the cell.
     * @param columnIdx coordinate y of the cell.
     * @param input user input number as a string.
     * @return bool, true if user input is present in the row, false otherwise.
     */

    public boolean inRow(int rowIdx, int columnIdx, String input){
        for(Integer num : getRowValues(rowIdx, columnIdx)){
            if(String.valueOf(num).equals(input)){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a user input number is present in the column.
     * Called from {@link SudokuController#inputControl(TextField, TextArea, int, int)}.
     * @param rowIdx coordinate x of the cell.
     * @param columnIdx coordinate y of the cell.
     * @param input user input number as a string.
     * @return bool, true if user input is present in the column, false otherwise.
     */
    public boolean inColumn(int rowIdx, int columnIdx, String input){
            for (Integer num : getColumnValues(columnIdx, rowIdx)) {
                if (String.valueOf(num).equals(input)) {
                    return true;
                }
            }
            return false;
        }


    /**
     * Checks if a user input number is present in a given grid.
     * Called from {@link SudokuController#inputControl(TextField, TextArea, int, int)}.
     * @param rowIdx coordinate x of the cell.
     * @param columnIdx coordinate y of the cell.
     * @param input user input number as a string.
     * @return bool, true if user input is present in the grid, false otherwise.
     */

    public boolean inGrid(int rowIdx, int columnIdx, String input) {
        for(Integer num : getSquareValues(rowIdx, columnIdx)){
            if(String.valueOf(num).equals(input)){
                return true;
            }
        }
        return false;

    }


    /**
     * Deletes a user input number from the board if user decides to change it.
     *  Called from {@link SudokuController#inputControl(TextField, TextArea, int, int)}.
     * @param rowIdx coordinate x of the cell.
     * @param columnIdx coordinate y of the cell.
     */
    public void deleteInput(int rowIdx, int columnIdx) {
        sudokuBoard[rowIdx][columnIdx] = -1;

    }

    /**
     * Row Iterator used to check if number is present in a row.
     * @see #inRow(int, int, String).
     * @param row coordinate x til cell.
     * @param column coordinate y til cell.
     * @return an iterator for a row.
     */
    public Iterable<Integer> getRowValues(int row, int column){
        return new Iterable<Integer>() {
            @Override
            public Iterator<Integer> iterator() {
                return new Iterator<Integer>() {
                    private int counter = 0;
                    @Override
                    public boolean hasNext() {
                        return (counter < GRID_SIZE);
                    }

                    @Override
                    public Integer next() {
                        if(counter != column){
                            return sudokuBoard[row][counter++];
                        } else{
                            counter++;
                            return -1;
                        }
                    }
                };
            }
        };
    }

    /**
     * Row Iterator used to check if number is present in a column.
     * @see #inColumn(int, int, String).
     * @param row coordinate x til cell.
     * @param column coordinate y til cell.
     * @return an iterator for a column.
     */
    public Iterable<Integer> getColumnValues(int column, int row){
        return new Iterable<Integer>() {
            @Override
            public Iterator<Integer> iterator() {
                return new Iterator<Integer>() {
                    private int counter = 0;
                    @Override
                    public boolean hasNext() {
                        return (counter < GRID_SIZE);
                    }
                    @Override
                    public Integer next() {
                        if(counter != row){
                            return sudokuBoard[counter++][column];
                        } else{
                            counter++;
                            return -1;
                        }
                    }
                };
            }
        };
    }

    /**
     * Row Iterator used to check if number is present in a grid.
     * @see #inGrid(int, int, String).
     * @param rowIdx coordinate x til cell.
     * @param columnIdx coordinate y til cell.
     * @return an iterator for a grid.
     */
    public Iterable<Integer> getSquareValues(int rowIdx, int columnIdx){
        return new Iterable<Integer>() {
            @Override
            public Iterator<Integer> iterator() {
                return new Iterator<Integer>() {

                    int currentRow = ((rowIdx / PANE_SIZE) * PANE_SIZE);
                    int currentColumn = ((columnIdx / PANE_SIZE) * PANE_SIZE);
                    int counter = 0;

                    @Override
                    public boolean hasNext() {
                        return (counter < GRID_SIZE);
                    }

                    @Override
                    public Integer next() {
                        int result;
                        if(currentRow != rowIdx || currentColumn != columnIdx){
                            result = sudokuBoard[currentRow][currentColumn];
                        } else{
                            result = -1;
                        }
                        counter++;
                        currentColumn++;
                        if((counter % PANE_SIZE) == 0){
                            currentColumn = ((columnIdx / PANE_SIZE) * PANE_SIZE);
                            currentRow++;
                        }
                        return result;
                    }
                };
            }
        };
    }

    /**
     * Changes the numbers between 1 - 9 in the board loaded from the json file.
     * Does this by putting legal numbers into and a list and shuffle the list.
     * @see #SudokuLogic(int).
     */
    public void randomizeBoardNumbers(){
        if(sudokuBoard.length != 0) {
            Integer[] randArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
            List<Integer> randList = Arrays.asList(randArray);
            Collections.shuffle(randList);

            for (int square = 0; square < GRID_SIZE; square++) {
                for (int cell = 0; cell < GRID_SIZE; cell++) {
                    sudokuBoard[square][cell] = (sudokuBoard[square][cell] <= 0) ? sudokuBoard[square][cell] : randList.get(sudokuBoard[square][cell] - 1);
                }
            }
        }
    }

    /**
     * Scrambles the starting positions of the board.
     * This can either be by rotating the board x*90 degrees or by mirroring horizontally,
     *      vertically, or diagonally.
     * Called from constructor.
     * @see #SudokuLogic(int).
     */
    public void boardPositionScrambler(){
        // Alt. too Math.random
        int randomChoice = random.nextInt(7) + 1;

        switch (randomChoice){
            case 1:
                mirrorBoardHorizontally();
                break;
            case 2:
                mirrorBoardVertically();
                break;
            case 3:
                mirrorBoardDiagonalyBackslash();
                break;
            case 4:
                mirrorBoardDiagonalyFrontslash();
                break;
            case 5:
                rotate90Clockwise();
                break;
            case 6:
                rotate180Clockwise();
                break;
            case 7:
                rotate270Clockwise();
            break;
            default:
                /*no change*/
                break;
        }
    }

    /**
     * Mirrors the board around a vertical axis in the 5th column.
     * @see #boardPositionScrambler().
     */
    public void mirrorBoardVertically() {
        for (int y = 0; y < GRID_SIZE/2; y++){
            for (int x = 0; x < GRID_SIZE; x++){
                int tmpInt = sudokuBoard[x][ARRLEN-y];
                sudokuBoard[x][ARRLEN-y] = sudokuBoard[x][y];
                sudokuBoard[x][y] = tmpInt;
            }
        }
    }

    /**
     * Mirrors the board around a horizontal axis in the 5th axis.
     * @see #boardPositionScrambler().
     */
    public void mirrorBoardHorizontally() {
        for (int y = 0; y < GRID_SIZE; y++) {
            for (int x = 0; x < GRID_SIZE / 2; x++) {
                int tmpInt = sudokuBoard[ARRLEN - x][y];
                sudokuBoard[ARRLEN - x][y] = sudokuBoard[x][y];
                sudokuBoard[x][y] = tmpInt;
            }
        }
    }

    /**
     * Mirrors the board around an diagonal axis with start and end positions m(1,1) and m(9,9).
     * @see #boardPositionScrambler().
     * | \|
     */
    public void mirrorBoardDiagonalyBackslash() {
        for (int y = 0; y < GRID_SIZE - 1;y++){
            for (int x = y + 1; x < GRID_SIZE; x++){
                int tmpInt = sudokuBoard[y][x];
                sudokuBoard[y][x] = sudokuBoard[x][y];
                sudokuBoard[x][y] = tmpInt;
            }
        }
    }

    /**
     * Mirrors the board around an diagonal axis with start and end positions m(1, 9) and m(9,1).
     * @see #boardPositionScrambler().
     * |/ |
     */
    public void mirrorBoardDiagonalyFrontslash() {
        for (int y = 0; y < GRID_SIZE - 1;y++){
            for (int x = 0; x < GRID_SIZE - y; x++){
                int tmpInt = sudokuBoard[ARRLEN - y][ARRLEN - x];
                sudokuBoard[ARRLEN - y][ ARRLEN - x] = sudokuBoard[x][y];
                sudokuBoard[x][y] = tmpInt;
            }
        }
    }

    /**
     * Rotates the board 90 degrees clockwise.
     * @see #boardPositionScrambler().
     */
    public void rotate90Clockwise() {
        mirrorBoardHorizontally();
        mirrorBoardDiagonalyBackslash();
    }

    /**
     * Rotates the board 90 degrees clockwise.
     * @see #boardPositionScrambler().
     */
    public void rotate180Clockwise() {
        mirrorBoardDiagonalyBackslash();
        mirrorBoardDiagonalyFrontslash();
    }

    /**
     * Rotates the board 270 degrees clockwise
     * @see #boardPositionScrambler().
     */
    public void rotate270Clockwise() {
        mirrorBoardHorizontally();
        mirrorBoardDiagonalyFrontslash();
    }

    /**
     * returns a single position on the board.
     * @param row chosen row
     * @param column chosen column
     * @return board position
     */
    protected int getValueInPosition(int row, int column) {
        return sudokuBoard[row][column];
    }

    /**
     * Checks if specified array value is constant
     * @param rowId row index in sudokuBoard array
     * @param columnId column index in sudokuBoard array
     * @throws BoardConsistencyException if the number that is attempted to change is constant
     */
    private void checkIfPosNotChangeble(int rowId, int columnId) throws BoardConsistencyException {
        if(lockedBoard[rowId][columnId] != -1){
            throw new BoardConsistencyException(rowId, columnId, sudokuBoard[rowId][columnId]);
        }
    }

    /**
     * Saves current game progress to file, player can load it anytime and continue
     * @param filename name with which to save the file with
     */
    protected void saveGameToJson(String filename){

        String name = filename;
        if("".equals(filename)){
            name = "default";
        }

        JSONObject json = new JSONObject();
        json.put("time", getTimeElapsed());
        json.put(BOARD, sudokuBoard);
        json.put("locked", lockedBoard);
        String jsonString = json.toString();
        try{
            PrintWriter out = new PrintWriter(new FileWriter("./savedGames/" + name + ".json"));
            out.write(jsonString);
            out.close();
        } catch (Exception ex){
            LOGGER.setLevel(Level.WARNING);
            LOGGER.warning("Save game failed to write file");
            LOGGER.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    /**
     * Loads a savefile and popualtes sudokuBoard and lockedBoard arrays
     * @param filename specifies the name of the file to load the save from
     */
    protected void loadGameFromJson(String filename){
        String name = filename;
        if("".equals(filename)){
            name = "default";
        }
        try(BufferedReader reader = new BufferedReader(new FileReader("./savedGames/" + name + ".json"))){
            StringBuilder localJsonBuilder = new StringBuilder();
            String currentLine = null;
            while((currentLine = reader.readLine()) != null){
                localJsonBuilder.append(currentLine);
            }
            JSONObject obj = new JSONObject(localJsonBuilder.toString());

            JSONArray sudokuArray = obj.getJSONArray(BOARD);
            JSONArray lockArray = obj.getJSONArray("locked");
            //int time = obj.getInt("time");

            for(int row = 0; row < GRID_SIZE; row++){
                JSONArray squareArray = sudokuArray.getJSONArray(row);
                for(int column = 0; column < GRID_SIZE; column++){
                    sudokuBoard[row][column] = squareArray.getInt(column);
                }
            }
            for(int row = 0; row < GRID_SIZE; row++){
                JSONArray squareArray = lockArray.getJSONArray(row);
                for(int column = 0; column < GRID_SIZE; column++){
                    lockedBoard[row][column] = squareArray.getInt(column);
                }
            }
        } catch(IOException ex){
            LOGGER.setLevel(Level.WARNING);
            LOGGER.warning("Load Game failed to load file");
            LOGGER.log(Level.WARNING, ex.getMessage(), ex);
        }
    }
}