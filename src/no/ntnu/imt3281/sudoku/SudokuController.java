package no.ntnu.imt3281.sudoku;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;

/**
 * Controller that controls GUI of the main window og the application.
 * this includes building the actual sudoku board, and the functionality of buttons and menus.
 */
public class SudokuController {

    private static final Logger LOGGER = SudokuLogger.getLogger();

    // Const in regards to length
    private static final int PANE_SIZE = 3;
    private static final int NO_OF_PANE = 9;
    private static final int CELL_NUM = 9;

    private static final int GRID_SIZE = 9;

    // Coloring
    private static final String GREY = "-fx-background-color: rgba(228, 233, 237, 1);";
    private static final String GREYINNERSHADOW = "-fx-effect: innershadow(one-pass-box, gray, 5, 0.3, 0, 0);";
    private static final String GREYBCKRADIUS =  "-fx-background-radius: 6;";
    private static final String WHITE =  "-fx-background-color: white";

    // REGEX pattern for textfields
    private static final Pattern TEXTFIELDPATTERN = Pattern.compile("^$|[1-9]");

    // Handling I18N
    private ResourceBundle resourceBundle;
    private String language = "";
    private String country = "";
    private int currentDifficulty = 0;

    // Is here in this shape to avoid code smells in sonarQube
    private static final String KEYCOUNTRY = "country";
    private static final String KEYLANGUAGE = "language";
    private static final String KEYDIFFICULTY = "chosen_difficulty";

    // Handling logic regarding the sudoku board
    private SudokuLogic logic;

    // variables for the GUI sudoku area
    private TextField[][] textFields;


    // Variables from the fxml file
    @FXML
    private GridPane root;

    @FXML
    private Menu menubarFile;

    @FXML
    private MenuItem menuFileClose;

    @FXML
    private Menu menubarRules;

    @FXML
    private MenuItem menuRulesShowRules;

    @FXML
    private Menu menubarLanguages;

    @FXML
    private MenuItem menuLanguageEN;

    @FXML
    private MenuItem menuLanguageET;

    @FXML
    private MenuItem menuLanguageNO;

    @FXML
    private MenuItem menuLanguagePL;

    @FXML
    private TextArea textareaWrongNumber;

    @FXML
    private Button buttonNewGame;

    @FXML
    private Label labelShowErrors;

    @FXML
    private CheckBox checkboxErrorHighlight;

    @FXML
    private Label labelDifficulty;

    @FXML
    private ChoiceBox choiceBoxDifficulty;

    @FXML
    private Button buttonDifficultyNewGame;

    @FXML
    private Label labelLoadGame;

    @FXML
    private ChoiceBox choiceBoxLoadGame;

    @FXML
    private Button buttonLoadGame;

    @FXML
    private Label labelSaveGame;

    @FXML
    private TextField textFieldSaveGame;

    @FXML
    private Button buttonSaveGame;

    ObservableList<String> savedGamesObservableList;

    /**
     * <p>Initialize the GUI. Puts into a 3x3 gridPane another 3x3 gridPane.</p>
     * <p>It is then put a textField into each gridPane in the second gridPane layer.</p>
     * <p>Sets a button to reset the game and a textfield to give message to the user.</p>
     * <p>Initializes a listener to track the change in difficulty level.</p>
     * <p>Creates option to save a game.</p>
     */
    @FXML
    void initialize(){

        //Sets listener for the choicebox, so that difficulty settings will be automatically updated
        checkboxErrorHighlight.setSelected(true);

        choiceBoxDifficulty.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue observableValue, Object o, Object t1)->
            currentDifficulty = choiceBoxDifficulty.getSelectionModel().getSelectedIndex()
        );

        savedGamesObservableList = FXCollections.observableArrayList();
        fillSavedGamesList();
        choiceBoxLoadGame.setItems(savedGamesObservableList);

        //Loads saved game settings and initialises menus and buttons
        loadSudokuGameProperties();
        resetMenusAndButtons();


        // Sets up the sudoku board inside
        logic = Sudoku.newLogic(currentDifficulty);
        textFields = new TextField[NO_OF_PANE][CELL_NUM];
        root.setAlignment(Pos.CENTER);

        textareaWrongNumber.setEditable(false);


        //Builds up sudoku griden

        for (int panes = 0; panes < NO_OF_PANE; panes++){

            GridPane pane = new GridPane();
            pane.setPrefHeight(150);
            pane.setPrefWidth(150);
            pane.setAlignment(Pos.CENTER);

            GridPane.setRowIndex(pane, (panes / 3));
            GridPane.setColumnIndex(pane, (panes % 3));

            root.getChildren().add(pane);

            for (int cells = 0; cells < CELL_NUM; cells++){

                TextField textField = new TextField();
                textField.setPrefHeight(45);
                textField.setPrefWidth(45);
                textField.setAlignment(Pos.CENTER);
                textField.setEditable(true);

                textField.setTextFormatter(new TextFormatter<>(c->{
                    if (c.getControlNewText().matches(TEXTFIELDPATTERN.toString())){
                        return c;
                    }else{
                        return null;
                    }
                }));

                textFields[panes][cells] = textField;

                GridPane.setRowIndex(textField, (cells / 3 ));
                GridPane.setColumnIndex(textField, (cells % 3 ));

                pane.getChildren().add(textField);

                inputControl(textField, textareaWrongNumber, panes , cells);
            }
       }
        settCellValues(logic.getBoard(), logic.getLockedBoard());
    }

    /**
     * <p>It fills the cells as they are dynamically created by the controller.</p>
     * <p>Controller creates first the grids.</p>
     * <p>Cells are filled, set uneditable and get a grey background.</p>
     *
     * @param boardArray the sudoku board array.
     * @param lockedArray the locked array.
     */
    public void settCellValues(int[][] boardArray, int[][] lockedArray) {
        for(int row = 0; row < GRID_SIZE; row++){
            for(int column = 0; column < GRID_SIZE; column++){

                int pane = ((row / PANE_SIZE) * PANE_SIZE) + (column / PANE_SIZE);
                int paneCell = ((row * PANE_SIZE) % GRID_SIZE) + (column % PANE_SIZE);

                if(lockedArray[row][column] != -1){
                    textFields[pane][paneCell].setText(String.valueOf(boardArray[row][column]));
                    textFields[pane][paneCell].setEditable(false);
                    textFields[pane][paneCell].setStyle(GREY + GREYINNERSHADOW + GREYBCKRADIUS );
                } else if(boardArray[row][column] != -1 && lockedArray[row][column] < 0){
                    textFields[pane][paneCell].setText(String.valueOf(boardArray[row][column]));
                    textFields[pane][paneCell].setEditable(true);
                    textFields[pane][paneCell].setStyle(WHITE);
                    textFields[pane][paneCell].setStyle(GREY);
                    textFields[pane][paneCell].setStyle(GREYBCKRADIUS);
                } else {
                    textFields[pane][paneCell].setText("");
                    textFields[pane][paneCell].setEditable(true);
                    textFields[pane][paneCell].setStyle(WHITE);
                    textFields[pane][paneCell].setStyle(GREY);
                    textFields[pane][paneCell].setStyle(GREYBCKRADIUS);
                }
            }
        }
    }

    /**
     * The games a user can choose from the load game option.
     * @see #initialize().
     */
    private void fillSavedGamesList(){
        File[] files = new File("./savedGames").listFiles();
        // Assert fra sonarQube
        assert files != null;
        for(File file : files){
            if(file.isFile()){
                savedGamesObservableList.add(FilenameUtils.removeExtension(file.getName()));
            }
        }
    }

    /**
     * <p>Controls user input numbers.</p>
     * <p>It checks if a user input number is present in row, column or grid.</p>
     * <p>If present: cell gets a red background, and user get "invalid number" message.</p>
     * <p>If valid input: cell gets yellow background color, board array is updated. Cell still editable if user wants to change number.</p>
     * <p>If user deletes a valid input data he put earlier: the controller removes the number from the board array.</p>
     * Called from {@link SudokuController#initialize()}.
     * @param textField a cell in the sudoku game.
     * @param messageTextfield the textfield to give user a message.
     * @param paneIdx coordinate x of the cell.
     * @param paneCellIdx coordinate y of the cell.
     */
    public void inputControl(TextField textField, TextArea messageTextfield, int paneIdx , int paneCellIdx){

        textField.setOnKeyTyped(e->{
            String input = textField.getText();
            int rowIdx = ((paneIdx / PANE_SIZE) * PANE_SIZE) + (paneCellIdx / PANE_SIZE);
            int columnIdx = ((paneIdx % PANE_SIZE) * PANE_SIZE) + (paneCellIdx % PANE_SIZE);
            try{
                if(logic.inRow(rowIdx, columnIdx, input) || logic.inColumn(rowIdx, columnIdx, input) || logic.inGrid(rowIdx, columnIdx, input)){
                    logic.updateBoard(rowIdx, columnIdx, input);
                    messageTextfield.setText("INVALID NUMBER!!");
                    messageTextfield.setStyle( "-fx-background-color: rgba(190, 60, 60, 1)");

                // if user deletes the  input
                }else if( "".equals(input)){
                    messageTextfield.setStyle("-fx-background-color: yellow");
                    messageTextfield.setText("");
                    logic.deleteInput(rowIdx, columnIdx);

                // if input ok
                }else{
                    messageTextfield.setText("");
                    messageTextfield.setStyle("-fx-background-color: yellow");
                    logic.updateBoard(rowIdx, columnIdx, input);
                    if(logic.validateSudokuBoard()){
                        congratulationWindows();
                    }
                }
            } catch (BoardConsistencyException err){
                LOGGER.setLevel(Level.SEVERE);
                LOGGER.severe("Trying to set in a value into an position that is not allowed to change");
                LOGGER.log(Level.SEVERE,err.getMessage(), err);
            }

            //Updated styles on the textfields
                updateTextFieldStyles();
        });

    }

    /**
     * <p>Updates text fields styles of the textfields array automatically</p>
     * @see #inputControl(TextField, TextArea, int, int).
     */
    private void updateTextFieldStyles(){

        for(int pane = 0; pane < GRID_SIZE; pane++){
            for(int cell = 0; cell < GRID_SIZE; cell++){

                int rowIdx = ((pane / PANE_SIZE) * PANE_SIZE) + (cell / PANE_SIZE);
                int columnIdx = ((pane % PANE_SIZE) * PANE_SIZE) + (cell % PANE_SIZE);

                if("".equals(textFields[pane][cell].getText())){
                    textFields[pane][cell].setStyle(WHITE);
                    textFields[pane][cell].setStyle(GREY);
                    textFields[pane][cell].setStyle(GREYBCKRADIUS);
                } else if(!checkboxErrorHighlight.isSelected()){
                    if(textFields[pane][cell].isEditable()){
                        textFields[pane][cell].setStyle(";");
                    } else{
                        textFields[pane][cell].setStyle(GREY + GREYINNERSHADOW+ GREYBCKRADIUS );
                    }
                } else{
                    if(logic.inRow(rowIdx, columnIdx, textFields[pane][cell].getText()) || logic.inColumn(rowIdx, columnIdx, textFields[pane][cell].getText()) || logic.inGrid(rowIdx, columnIdx, textFields[pane][cell].getText())){
                        if(!textFields[pane][cell].isEditable()){
                            textFields[pane][cell].setStyle("-fx-background-color: rgba(228, 50, 0, 0.85); " + GREYINNERSHADOW + GREYBCKRADIUS );
                        } else{
                            textFields[pane][cell].setStyle("-fx-background-color: red; " + GREYINNERSHADOW + GREYBCKRADIUS);
                        }
                    } else{
                        if(textFields[pane][cell].isEditable()){
                            textFields[pane][cell].setStyle("-fx-background-color: rgba(53, 182, 100, 1); " + GREYINNERSHADOW + GREYBCKRADIUS);
                        } else{
                            textFields[pane][cell].setStyle(GREY + GREYINNERSHADOW+ GREYBCKRADIUS );
                        }
                    }
                }
            }
        }
    }

    /**
     * Creates the window which says congratulation to the user.
     * @see #inputControl(TextField, TextArea, int, int).
     */
    private void congratulationWindows(){
        Stage congratulationStage = new Stage();

        congratulationStage.initStyle(StageStyle.UNDECORATED);
        congratulationStage.initStyle(StageStyle.TRANSPARENT);
        congratulationStage.initModality(Modality.WINDOW_MODAL);
        congratulationStage.initOwner(root.getScene().getWindow());

        Sudoku.loadVictoryScreen();

        Scene victoryScene = Sudoku.loadVictoryScreen();
        victoryScene.setFill(Color.TRANSPARENT);
        congratulationStage.setTitle(resourceBundle.getString("victoryTitle"));
        congratulationStage.setScene(victoryScene);
        saveSudokuGameProperties();
        congratulationStage.setOnHiding(evt -> buttonNewGameWhenPressed(new ActionEvent()) );
        congratulationStage.show();
    }

    // Functions from Scenebuilder
    /**
     * <p>Starts a new game when button named "new game" button on lower right is pressed.</p>
     * <p>Fills a sudoku game and empties the textarea used to give user a message. </p>
     * @param event information about the button pressed.
     */
    @FXML
    void buttonNewGameWhenPressed(ActionEvent event) {
        logic = Sudoku.newLogic(currentDifficulty);

        settCellValues(logic.getBoard(), logic.getLockedBoard());

        textareaWrongNumber.setText("");
        textareaWrongNumber.setStyle(WHITE);
        textareaWrongNumber.setStyle(GREY);
    }

    /**
     * saves currents settings, and then closes the app when pressed.
     * @param event information about the menuItem pressed.
     */
    @FXML
    void closeWhenPressed(ActionEvent event) {
        saveSudokuGameProperties();
        System.exit(0);
    }

    /**
     * when pressed a new window pops up and explain the rules.
     * @param event information about the menuItem pressed.
     */
    @FXML
    void showRulesWhenPressed(ActionEvent event) {
        Stage rulesStage = new Stage();
        Pane rulesPane = new Pane();
        rulesPane.setPrefWidth(300);
        rulesPane.setPrefHeight(200);


        Label rulesLabel = new Label();
        rulesLabel.setText(resourceBundle.getString("rulesExplanation"));
        rulesLabel.setAlignment(Pos.CENTER);
        rulesLabel.setMaxWidth(300);
        rulesLabel.setPadding(new Insets(15,15,15,15));
        rulesLabel.setWrapText(true);
        rulesPane.getChildren().add(rulesLabel);

        Scene rulesScene = new Scene(rulesPane);
        rulesStage.setTitle(resourceBundle.getString("menubarRulesRules"));
        rulesStage.setScene(rulesScene);
        rulesStage.show();
    }

    /**
     * when menuItem is pressed the function changes thae variables language and country,
     * then resets the language of the GUI to english.
     * @param event information about the menuItem pressed.
     */
    @FXML
    void changeLanguageToEN(ActionEvent event) {
        language = "en";
        country = "EN";
        resetMenusAndButtons();
    }

    /**
     * when menuItem is pressed the function changes thae variables language and country,
     * then resets the language of the GUI to ethiopian.
     * @param event information about the menuItem pressed.
     */
    @FXML
    void changeLanguageToET(ActionEvent event) {
        language = "et";
        country = "ET";
        resetMenusAndButtons();
    }

    /**
     * when menuItem is pressed the function changes thae variables language and country,
     * then resets the language of the GUI to norwegian.
     * @param event information about the menuItem pressed.
     */
    @FXML
    void changeLanguageToNO(ActionEvent event) {
        language = "no";
        country = "NO";
        resetMenusAndButtons();
    }

    /**
     * when menuItem is pressed the function changes thae variables language and country,
     * then resets the language of the GUI to polish.
     * @param event information about the menuItem pressed.
     */
    @FXML
    void changeLanguageToPL(ActionEvent event) {
        language = "pl";
        country = "PL";
        resetMenusAndButtons();
    }

    /**
     * Support function that reset all buttons, labels, and menus to a chosen properties file.
     *
     * Based of Ø. Kolloen's Internationalization
     * @url https://bitbucket.org/okolloen/imt3281_h2019/src/master/forelesning/36/
     * Used in the following functions:
     *   changeLanguageToEN
     *   changeLanguageToET
     *   changeLanguageToNO
     *   changeLanguageToPL
     */
    private void resetMenusAndButtons(){
        // Decides new Locale
        if ("".equals(language) && "".equals(country)){
            Sudoku.setCurrentGlobalLocale(Locale.getDefault());
        } else if ( "en".equals(language) && "EN".equals(country) ){
            Sudoku.setCurrentGlobalLocale(new Locale(language, country));
        } else if ("et".equals(language) && "ET".equals(country)){
            Sudoku.setCurrentGlobalLocale(new Locale(language, country));
        } else if ("no".equals(language) && "NO".equals(country)){
            Sudoku.setCurrentGlobalLocale(new Locale(language, country));
        } else if ("pl".equals(language) && "PL".equals(country)){
            Sudoku.setCurrentGlobalLocale(new Locale(language, country));
        }
        // get the correct resource bundle
        resourceBundle = ResourceBundle.getBundle("Messages", Sudoku.getCurrentLocale());

        // makes the correct changes

        int tempdiff = currentDifficulty;
        ObservableList<String> difficultyList = FXCollections.observableArrayList(resourceBundle.getString("difficulty_easy"), resourceBundle.getString("difficulty_medium"), resourceBundle.getString("difficulty_hard"));
        choiceBoxDifficulty.setItems(difficultyList);
        currentDifficulty = tempdiff;
        choiceBoxDifficulty.getSelectionModel().select(currentDifficulty);

        menubarFile.setText(resourceBundle.getString("menubarFile"));
        menuFileClose.setText(resourceBundle.getString("menubarFileClose"));

        menubarRules.setText(resourceBundle.getString("menubarRules"));
        menuRulesShowRules.setText(resourceBundle.getString("menubarRulesRules"));

        menubarLanguages.setText(resourceBundle.getString("menubarLanguages"));
        menuLanguageEN.setText(resourceBundle.getString("menubarLanguageEN"));
        menuLanguageET.setText(resourceBundle.getString("menubarLanguageET"));
        menuLanguageNO.setText(resourceBundle.getString("menubarLanguageNO"));
        menuLanguagePL.setText(resourceBundle.getString("menubarLanguagePL"));

        buttonNewGame.setText(resourceBundle.getString("buttonNewGame"));

        labelShowErrors.setText(resourceBundle.getString("labelShowErrorsText"));
        checkboxErrorHighlight.setText(resourceBundle.getString("checkboxErrorHighlightLabel"));

        labelDifficulty.setText(resourceBundle.getString("labelDifficultyText"));
        buttonDifficultyNewGame.setText(resourceBundle.getString("buttonDifficultyNewGameText"));

        labelLoadGame.setText(resourceBundle.getString("labelLoadGameText"));
        buttonLoadGame.setText(resourceBundle.getString("buttonLoadGameText"));
        labelSaveGame.setText(resourceBundle.getString("labelSaveGamedText"));
        buttonSaveGame.setText(resourceBundle.getString("buttonSaveGameText"));
        textFieldSaveGame.setPromptText(resourceBundle.getString("textFieldSaveGamePrompt"));

    }

    /**
     * Saves language and difficulty settings for the application in settings.properties
     *
     * Based of the tutorial of MKyong.
     * @url https://www.mkyong.com/java/java-properties-file-examples/
     */
    protected void saveSudokuGameProperties(){
        try (OutputStream outputStream = new FileOutputStream("resources/config/settings.properties")){
            Properties properties = new Properties();
            properties.setProperty(KEYCOUNTRY, country);
            properties.setProperty(KEYLANGUAGE, language);
            properties.setProperty(KEYDIFFICULTY, Integer.toString(currentDifficulty));
            properties.store(outputStream, null);

            // Logger used to check if change occurred
            LOGGER.setLevel(Level.FINE);
            LOGGER.info("Settings saved to: " + properties.getProperty(KEYLANGUAGE) + "_"
                    + properties.getProperty(KEYCOUNTRY) + "_"
                    + properties.getProperty(KEYDIFFICULTY));
        } catch (IOException e){
            LOGGER.setLevel(Level.WARNING);
            LOGGER.warning("Could not save settings");
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Loads language and difficulty sesttings for the application from settings.properties
     * Called from {@link SudokuController#initialize()}.
     *
     * Based of the tutorial of MKyong.
     * @url https://www.mkyong.com/java/java-properties-file-examples/
     */
    protected void loadSudokuGameProperties(){
        try (InputStream inputStream = new FileInputStream("resources/config/settings.properties");){

            Properties properties = new Properties();
            properties.load(inputStream);
            language = properties.getProperty(KEYLANGUAGE, "");

            country = properties.getProperty(KEYCOUNTRY, "");

            currentDifficulty = Integer.parseInt(properties.getProperty(KEYDIFFICULTY, "0"));
            if(currentDifficulty < 0){
                currentDifficulty = 0;
            }

            // Logger used to check if change occurred
            LOGGER.setLevel(Level.FINE);
            LOGGER.info("Settings Loaded: " + properties.getProperty(KEYLANGUAGE)
                    + "_" + properties.getProperty(KEYCOUNTRY)
                    + "_" + properties.getProperty(KEYDIFFICULTY));
        } catch (IOException e){
            LOGGER.setLevel(Level.WARNING);
            LOGGER.info("Could not load saved language and/or difficulty from settings.properties");
            LOGGER.log(Level.WARNING, e.getMessage(), e);
        }

    }

    /**
     * Called when user clicks on checkboxErrorHighlight checkbox
     * Forces textfield style update
     * @param evt information about the event
     */
    @FXML
    public void checkboxErrorHighlightPressed(ActionEvent evt){
        updateTextFieldStyles();
    }

    /**
     * Called when user presses buttonDifficultyNewGame button, it will generate new game with the specified difficulty.
     * @param evt Information about the event
     */
    @FXML
    public void buttonDifficultyNewGamePressed(ActionEvent evt){
        logic = Sudoku.newLogic(currentDifficulty);

        settCellValues(logic.getBoard(), logic.getLockedBoard());

        textareaWrongNumber.setText("");
        textareaWrongNumber.setStyle(WHITE);
        textareaWrongNumber.setStyle(GREY);
    }

    /**
     * Saves the game when a user presses the button "save game".
     * @param evt the event of the save game button.
     */
    @FXML
    public void buttonSaveGamePressed(ActionEvent evt){
        String name = textFieldSaveGame.getText();
        logic.saveGameToJson(name);
        if(!savedGamesObservableList.contains(name)){
            savedGamesObservableList.add(name);
        }
    }

    /**
     * Loads a chosen game when a user presses the button "load game".
     * @param evt the event of the load game button.
     */
    @FXML
    public void buttonLoadGamePressed(ActionEvent evt){

        int idx = choiceBoxLoadGame.getSelectionModel().getSelectedIndex();

        if(idx != -1){

            String name = choiceBoxLoadGame.getSelectionModel().getSelectedItem().toString();
            logic.loadGameFromJson(name);

            settCellValues(logic.getBoard(), logic.getLockedBoard());
            updateTextFieldStyles();

            textareaWrongNumber.setText("");
            textareaWrongNumber.setStyle(WHITE);
            textareaWrongNumber.setStyle(GREY);
        }
    }

}
