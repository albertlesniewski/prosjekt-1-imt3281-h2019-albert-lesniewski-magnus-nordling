package no.ntnu.imt3281.sudoku;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *<p>This application is a sudoku game application. </p>
 *<p>It has also functionalities to change the layout of the board {@link SudokuLogic#boardPositionScrambler()}. </p>
 *
 *<p>{@link SudokuLogic} is the logic behind the sudoku game. It has the functions to check user input and update the board </p>
 *<p>{@link SudokuController} contains the code that process the sudoku main application window.</p>
 *
 */
public class Sudoku extends Application {

    private static SudokuLogic logic;
    private static Locale currentGlobalLocale;
    private static final Logger LOGGER = SudokuLogger.getLogger();

    /**
     * <p>Starts the sudoku application.</p>
     *
     * @param stage the window for this application
     * @see javafx.application.Application#start(Stage)
     * @throws IOException if unable to load the fxml file
     */
    @Override
    public void start(Stage stage) throws IOException {
        ResourceBundle bundle = ResourceBundle.getBundle("Messages", Locale.getDefault());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("sudoku.fxml"), bundle);
        Parent root = loader.load();
        SudokuController controller = loader.getController();
        Scene  scene = new Scene(root);
        stage.setTitle("Sudoku");
        stage.setScene(scene);
        stage.setMinHeight(600);
        stage.setMinWidth(800);
        stage.setOnHiding(evt ->
            controller.saveSudokuGameProperties()
        );
        stage.show();
    }

    /**
     *
     * <p>gets current language settings of the application</p>
     *Called from {@link CongratulationWindowController#initialize()}.
     * @return Locale

     */
    public static Locale getCurrentLocale(){
        return currentGlobalLocale;
    }


    /**
     *
     * <p>Sets new language setting to the application</p>
     *
     * @param newLocale specifies new language setting of the application
     */
    public static void setCurrentGlobalLocale(Locale newLocale){
        currentGlobalLocale = newLocale;
    }

    /**
     * Gets the current instance of {@link SudokuLogic} that is currently in use
     *
     * @return {@link SudokuLogic} is the logic behind the sudoku game. It has the functions to check user input and update the board
     */
    public static SudokuLogic getLogic(){
        return logic;
    }

    /**
     * Creates new instance of {@link SudokuLogic} with new board
     *
     * @param difflvl specifies desired difficulty level of the new game
     * @return {@link SudokuLogic} is the logic behind the sudoku game. It has the functions to check user input and update the board
     */
    public static SudokuLogic newLogic(int difflvl){
        logic = new SudokuLogic(difflvl);
        return logic;
    }

    /**
     * Created and return the new scene of the victory screen
     *
     * @return {@link Scene} The new scene of the victory screen
     */
    public static Scene loadVictoryScreen() {
        try{
            ResourceBundle bundle = ResourceBundle.getBundle("Messages", Locale.getDefault());
            return new Scene(FXMLLoader.load(Sudoku.class.getResource("congratulation_window.fxml"), bundle));
        } catch(IOException err){
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.severe("Can not load victoryscreen");
            LOGGER.log(Level.SEVERE, err.getMessage(), err);
        }
        return null;
    }

    /**
     * Starts the application by calling {@link javafx.application.Application#launch(String...)}.
     *
     * @param args not used.
     */
    public static void main(String[] args) {
        launch();
    }
}