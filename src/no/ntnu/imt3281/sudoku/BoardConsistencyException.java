package no.ntnu.imt3281.sudoku;

/**
 * Class that checks board consistency.
 */
public class BoardConsistencyException extends Exception {
    private final int roxId;
    private final int columnId;
    private final int value;
    private static final String MESSAGE = "";

    /**
     *
     *   Constructs a new exception with {@code null} as its detail MESSAGE.
     *   The cause is not initialized, and may subsequently be initialized by a
     *   call to {@link #initCause}.
     * @param roxId coordinate x til cell.
     * @param columnId coordinate y til cell.
     * @param value number in a cell with those coordinate.
     */
    public BoardConsistencyException(int roxId, int columnId, int value) {
        this.roxId = roxId;
        this.columnId = columnId;
        this.value = value;
    }

    @Override
    public String toString() {
        return "BoardConsistityException{" +
                "roxId=" + roxId +
                ", columnId=" + columnId +
                ", value=" + value +
                ", MESSAGE='" + MESSAGE + '\'' +
                '}';
    }
}
