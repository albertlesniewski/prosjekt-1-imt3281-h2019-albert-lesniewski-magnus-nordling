package no.ntnu.imt3281.sudoku;

import org.junit.Assert;
import org.junit.Test;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * General tests for sudoku functionality
 *
 */


public class SudokuLogicTest {
    // Enum used to select correct board to test changes up against.
    enum BoardNames {standard, ansVertical, ansHorizontal, ansDiagonalBackslah,
            ansDiagonalFrontslash, ansRot90Clockwise, ansRot180Clockwise, ansRot270Clockwise};


    @Test
    public void testBoardSize(){
        SudokuLogic sudoku = new SudokuLogic();
        assertEquals(81, sudoku.getBoardSize());
    }

    @Test
    public void testLoadBoard(){
        SudokuLogic sudoku = new SudokuLogic();
        int[][] template1 = boardCollection(BoardNames.standard);
        sudoku.loadBoard("board1.json");
        Assert.assertArrayEquals(template1, sudoku.getBoard());
    }

    @Test
    public void testGetValueInPosition(){
        SudokuLogic sudoku = new SudokuLogic();
        Assert.assertEquals(5, sudoku.getValueInPosition(0, 0));
        Assert.assertEquals(9, sudoku.getValueInPosition(8, 8));
        Assert.assertEquals(6, sudoku.getValueInPosition(3, 4));
        Assert.assertNotEquals(1, sudoku.getValueInPosition(1, 8));
    }

    @Test
    public void testUpdateBoard(){
        SudokuLogic sudoku = new SudokuLogic();
        int newTestValue = 2;
        Assert.assertNotEquals(newTestValue, sudoku.getValueInPosition(2, 3));
        try{
            sudoku.updateBoard(2, 3, Integer.toString(newTestValue));
        }
        catch(BoardConsistencyException err){
            System.out.println("Board Consistency Exception - Error");
        }
        Assert.assertEquals(newTestValue, sudoku.getValueInPosition(2, 3));
    }

    @Test
    public void testBoardRowIterator(){
        SudokuLogic sudoku = new SudokuLogic();
        int[] testarr = new int[9];
        int[] finalarr = {5, 3, -1, -1, 7, -1, -1, -1, -1};
        int idx = 0;
        for(Integer v : sudoku.getRowValues(0, 5)){
            testarr[idx++] = v;
        }
        Assert.assertArrayEquals(finalarr, testarr);
    }

    @Test
    public void testBoardColumnIterator(){
        SudokuLogic sudoku = new SudokuLogic();
        int[] testarr = new int[9];
        int[] finalarr = {-1, -1, 9, -1, -1, -1, 6, -1, -1};
        int idx = 0;
        for(Integer v : sudoku.getColumnValues(1, 0)){
            testarr[idx++] = v;
        }
        Assert.assertArrayEquals(finalarr, testarr);
    }

    @Test
    public void testBoardSquareIterator(){
        SudokuLogic sudoku = new SudokuLogic();
        int[] testarr = new int[9];
        int[] finalarr = {5, 3, -1, 6, -1, -1, -1, 9, 8};
        int idx = 0;
        for(Integer v : sudoku.getSquareValues(0, 2)){
            testarr[idx++] = v;
        }
        Assert.assertArrayEquals(finalarr, testarr);
    }

    /**
     * Test to see if a board is mirrored around a vertical axis in the 5th column
     */
    @Test
    public void testMirrorBoardVertically(){
        SudokuLogic sudokuLogic = new SudokuLogic();
        sudokuLogic.loadBoard("board1.json");
        int[][] answer = boardCollection(BoardNames.ansVertical);
        sudokuLogic.mirrorBoardVertically();
        Assert.assertArrayEquals(answer, sudokuLogic.getBoard());
    }

    /**
     * The test check if the board mirrors around a horizontal axis in the 5th row
     */
    @Test
    public void testMirrorMirroringHorizontally(){
        SudokuLogic sudokuLogic = new SudokuLogic();
        sudokuLogic.loadBoard("board1.json");
        int[][] answer = boardCollection(BoardNames.ansHorizontal);
        sudokuLogic.mirrorBoardHorizontally();
        Assert.assertArrayEquals(answer, sudokuLogic.getBoard());
    }

    /**
     * Tests if the board is correctly mirrored around an axis with start and end positions m(1,1) and m(9,9).
     * |\ |
     * | \|
     */
    @Test
    public void testMirroringAlongDiagonalBackslash(){
        SudokuLogic sudokuLogic = new SudokuLogic();
        sudokuLogic.loadBoard("board1.json");
        int [][] answer = boardCollection(BoardNames.ansDiagonalBackslah);
        sudokuLogic.mirrorBoardDiagonalyBackslash();
        Assert.assertArrayEquals(answer, sudokuLogic.getBoard());
    }

    /**
     * Tests if the board is correctly mirrored around an axis with start and end positions m(1, 9) and m(9, 1).
     * |  /|
     * |/ |
     */
    @Test
    public void testMirroringBoardDiagonallyFrontslash(){
        SudokuLogic sudokuLogic = new SudokuLogic();
        sudokuLogic.loadBoard("board1.json");
        int[][] answer = boardCollection(BoardNames.ansDiagonalFrontslash);
        sudokuLogic.mirrorBoardDiagonalyFrontslash();
        Assert.assertArrayEquals(answer, sudokuLogic.getBoard());
    }

    /**
     * Tests if the board has been rotated 90 degrees clockwise
     */
    @Test
    public void testRotate90DegClockWise(){
        SudokuLogic sudokuLogic = new SudokuLogic();
        sudokuLogic.loadBoard("board1.json");
        int[][] answer = boardCollection(BoardNames.ansRot90Clockwise);
        sudokuLogic.rotate90Clockwise();
        Assert.assertArrayEquals(answer, sudokuLogic.getBoard());
    }

    /**
     * Tests if the board has been rotated 180 degrees clockwise
     */
    @Test
    public void testRotate180Degrees(){
        SudokuLogic sudokuLogic = new SudokuLogic();
        sudokuLogic.loadBoard("board1.json");
        int[][] answer = boardCollection(BoardNames.ansRot180Clockwise);
        sudokuLogic.rotate180Clockwise();
        Assert.assertArrayEquals(answer, sudokuLogic.getBoard());
    }

    /**
     * Tests if the board has been rotated 270 degrees clockwise
     */
    @Test
    public void testRotate280Degrees(){
        SudokuLogic sudokuLogic = new SudokuLogic();
        sudokuLogic.loadBoard("board1.json");
        int[][] answer = boardCollection(BoardNames.ansRot270Clockwise);
        sudokuLogic.rotate270Clockwise();
        Assert.assertArrayEquals(answer, sudokuLogic.getBoard());
    }


    /**
     * Tests if a number is in a row.
     * Indexing based on order of creation of subgrids. (first subgrid has x = 0, 2nd x = 1,... subgrids created vertically)
     */
    @Test
    public void testInRow() {
        SudokuLogic number = new SudokuLogic();
        number.loadBoard("board1.json");
        assertTrue("Number in row index 0", number.inRow(0,2, "3"));
        assertTrue("Number in row index 3", number.inRow(4,1, "8"));
        assertTrue("Number in row index 8", number.inRow(8,8, "8"));
        assertFalse("Number not in row index 2", number.inRow(1,8, "7"));
    }

    /**
     * Tests if a number is in a column
     */

    @Test
    public void testInColumn() {
        SudokuLogic number = new SudokuLogic();
        number.loadBoard("board1.json");
        assertTrue("Number in column index 0", number.inColumn(2,0, "8"));
        assertTrue("Number in column index 3", number.inColumn(4,3, "4"));
        assertTrue("Number in column index 8", number.inColumn(8,8, "6"));
        assertFalse("Number not in column index 1", number.inColumn(6,1, "8"));
        assertFalse("Number not in column index 2", number.inColumn(3,2, "3"));
    }

    /**
     * Tests if a number is in a grid
     */
    @Test
    public void testInGrid() {
        SudokuLogic number = new SudokuLogic();
        number.loadBoard("board1.json");
        assertTrue("Number in grid 0", number.inGrid(0, 2, "6"));
        assertTrue("Number in grid 1", number.inGrid(2, 4, "9"));
        assertTrue("Number in grid 5", number.inGrid(4, 7, "3"));
        assertFalse("Number not in grid 5", number.inGrid(3, 8, "8"));
    }


    /**
     * Tests that a number is deleted from the board.
     */
    @Test
    public void testDeleteInput() {
        SudokuLogic logic = new SudokuLogic();
        logic.deleteInput(0,0);
        logic.deleteInput(0,1);
        logic.deleteInput(0,2);
        logic.deleteInput(0,3);

        int [][]board = logic.getBoard();
        assertEquals(-1, board[0][0]);
        assertEquals(-1, board[0][1]);
        assertEquals(-1, board[0][2]);
        assertEquals(-1, board[0][3]);

    }



    /**
     * Checks if the board loaded from file has been has
     */
    @Test
    public void testRandomizeBoardNumbers(){
        SudokuLogic sudokuLogic = new SudokuLogic();
        int[][] originalBoard = boardCollection(BoardNames.standard);
        sudokuLogic.randomizeBoardNumbers();                      // Rerandomizes the board

        Random random = new Random();
        int randomRow;
        int randomColumn;
        do{
            randomRow = random.nextInt(9);
            randomColumn = random.nextInt(9);
        }while(sudokuLogic.getValueInPosition(randomRow, randomColumn) < 0);

        int oldnumber = originalBoard[randomRow][randomColumn];
        int testNumber = sudokuLogic.getValueInPosition(randomRow, randomColumn);
        Assert.assertNotEquals(-1, testNumber);
        Assert.assertNotEquals(originalBoard, sudokuLogic.getBoard());

        // check if a known position with value -1 is not changed.
        Assert.assertEquals(originalBoard[0][2], sudokuLogic.getValueInPosition(0,2));

        //check all randomized positions
        for(int row = 0; row < 9; row++){
            for (int column = 0; column < 9; column++){
                if(originalBoard[row][column] == oldnumber){
                    Assert.assertEquals(sudokuLogic.getValueInPosition(row, column), testNumber);
                }
            }
        }
    }

    /**
     * Checks if BoardConsistencyException will be thorwn if somehow there will be an attempt to update board
     * on field that should be constant
     * @throws BoardConsistencyException
     */
    @Test(expected = BoardConsistencyException.class)
    public void testUpdateNotChangableField() throws BoardConsistencyException{
        SudokuLogic sudoku = new SudokuLogic();
        sudoku.updateBoard(0, 0, "2");
    }

    /**
     * Test if timer in logic works as desired.
     * @throws InterruptedException
     */
    @Test
    public void testGetTimeElapsed() throws InterruptedException {
        SudokuLogic sudokuLogic = new SudokuLogic();
        Thread.sleep(2000);
        int time = sudokuLogic.getTimeElapsed();
        Assert.assertEquals(2, time);
    }



    /**
     * Support function who sole purpose is to contain answers for the mirroring and clock
     * @param choice an int used in a switch choose correct answer boards
     * @return an answer board of size int[9][9]
     */
    private int[][] boardCollection(BoardNames choice){
        int[][] template = {
                { 5,  3, -1, -1,  7, -1, -1, -1, -1},
                { 6, -1, -1,  1,  9,  5, -1, -1, -1},
                {-1,  9,  8, -1, -1, -1, -1,  6, -1},
                { 8, -1, -1, -1,  6, -1, -1, -1,  3},
                { 4, -1, -1,  8, -1,  3, -1, -1,  1},
                { 7, -1, -1, -1,  2, -1, -1, -1,  6},
                {-1,  6, -1, -1, -1, -1,  2,  8, -1},
                {-1, -1, -1,  4,  1,  9, -1, -1,  5},
                {-1, -1, -1, -1,  8, -1, -1,  7,  9}};

        int[][] rotated270degClockwise = {
                {-1, -1, -1,  3,  1,  6, -1,  5,  9},
                {-1, -1,  6, -1, -1, -1,  8, -1,  7},
                {-1, -1, -1, -1, -1, -1,  2, -1, -1},
                {-1,  5, -1, -1,  3, -1, -1,  9, -1},
                { 7,  9, -1,  6, -1,  2, -1,  1,  8},
                {-1,  1, -1, -1,  8, -1, -1,  4, -1},
                {-1, -1,  8, -1, -1, -1, -1, -1, -1},
                { 3, -1,  9, -1, -1, -1,  6, -1, -1},
                { 5,  6, -1,  8,  4,  7, -1, -1, -1}};

        int[][] rotated180degClockwise = {
                { 9,  7, -1, -1,  8, -1, -1, -1, -1},
                { 5, -1, -1,  9,  1,  4, -1, -1, -1},
                {-1,  8,  2, -1, -1, -1, -1,  6, -1},
                { 6, -1, -1, -1,  2, -1, -1, -1,  7},
                { 1, -1, -1,  3, -1,  8, -1, -1,  4},
                { 3, -1, -1, -1,  6, -1, -1, -1,  8},
                {-1,  6, -1, -1, -1, -1,  8,  9, -1},
                {-1, -1, -1,  5,  9,  1, -1, -1,  6},
                {-1, -1, -1, -1,  7, -1, -1,  3,  5}};


        int[][] rotated90degClockwise = {
                {-1, -1, -1,  7,  4,  8, -1,  6,  5},
                {-1, -1,  6, -1, -1, -1,  9, -1,  3},
                {-1, -1, -1, -1, -1, -1,  8, -1, -1},
                {-1,  4, -1, -1,  8, -1, -1,  1, -1},
                { 8,  1, -1,  2, -1, 6, -1,  9,  7},
                {-1,  9, -1, -1,  3, -1, -1,  5, -1},
                {-1, -1,  2, -1, -1, -1, -1, -1, -1},
                { 7, -1,  8, -1, -1, -1,  6, -1, -1},
                { 9,  5, -1,  6,  1,  3, -1, -1, -1}};

        int[][] diagonalFrontslash = {
                { 9,  5, -1,  6,  1,  3, -1, -1, -1},
                { 7, -1,  8, -1, -1, -1,  6, -1, -1},
                {-1, -1,  2, -1, -1, -1, -1, -1, -1},
                {-1,  9, -1, -1,  3, -1, -1,  5, -1},
                { 8,  1, -1,  2, -1,  6, -1,  9,  7},
                {-1,  4, -1, -1,  8, -1, -1,  1, -1},
                {-1, -1, -1, -1, -1, -1,  8, -1, -1},
                {-1, -1,  6, -1, -1, -1,  9, -1,  3},
                {-1, -1, -1,  7,  4,  8, -1,  6,  5}};

        int[][] diagonalBackslash =    {
                { 5,  6, -1,  8,  4,  7, -1, -1, -1},
                { 3, -1,  9, -1, -1, -1,  6, -1, -1},
                {-1, -1,  8, -1, -1, -1, -1, -1, -1},
                {-1,  1, -1, -1,  8, -1, -1,  4, -1},
                { 7,  9, -1,  6, -1,  2, -1,  1,  8},
                {-1,  5, -1, -1,  3, -1, -1,  9, -1},
                {-1, -1, -1, -1, -1, -1,  2, -1, -1},
                {-1, -1,  6, -1, -1, -1,  8, -1,  7},
                {-1, -1, -1,  3,  1,  6, -1,  5,  9}};

        int[][] mirrorHorizontal = {
                {-1, -1, -1, -1,  8, -1, -1,  7,  9},
                {-1, -1, -1,  4,  1,  9, -1, -1,  5},
                {-1,  6, -1, -1, -1, -1,  2,  8, -1},
                { 7, -1, -1, -1,  2, -1, -1, -1,  6},
                { 4, -1, -1,  8, -1,  3, -1, -1,  1},
                { 8, -1, -1, -1,  6, -1, -1, -1,  3},
                {-1,  9,  8, -1, -1, -1, -1,  6, -1},
                { 6, -1, -1,  1,  9,  5, -1, -1, -1},
                { 5,  3, -1, -1,  7, -1, -1, -1, -1}};

        int[][] mirrorVertical = {
                {-1, -1, -1, -1,  7, -1, -1,  3,  5},
                {-1, -1, -1,  5,  9,  1, -1, -1,  6},
                {-1,  6, -1, -1, -1, -1,  8,  9, -1},
                { 3, -1, -1, -1,  6, -1, -1, -1,  8},
                { 1, -1, -1,  3, -1,  8, -1, -1,  4},
                { 6, -1, -1, -1,  2, -1, -1, -1,  7},
                {-1,  8,  2, -1, -1, -1, -1,  6, -1},
                { 5, -1, -1,  9,  1,  4, -1, -1, -1},
                { 9,  7, -1, -1,  8, -1, -1, -1, -1}};

        int[][] emptyBoard = {
                {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1}};

        switch (choice){
            case standard:              return template;
            case ansHorizontal:         return mirrorHorizontal;
            case ansVertical:           return mirrorVertical;
            case ansDiagonalBackslah:   return diagonalBackslash;
            case ansDiagonalFrontslash: return diagonalFrontslash;
            case ansRot90Clockwise:     return rotated90degClockwise;
            case ansRot180Clockwise:    return rotated180degClockwise;
            case ansRot270Clockwise:    return rotated270degClockwise;
        }
        return emptyBoard;
    }
}
